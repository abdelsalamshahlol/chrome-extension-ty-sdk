$(document).ready(function() {
  //this block is used to create the pretty looking clock
  function get_elapsed_time_string(total_seconds) {
    function pretty_time_string(num) {
      return (num < 10 ? "0" : "") + num;
    }

    var hours = Math.floor(total_seconds / 3600);
    total_seconds = total_seconds % 3600;

    var minutes = Math.floor(total_seconds / 60);
    total_seconds = total_seconds % 60;

    var seconds = Math.floor(total_seconds);

    // Pad the minutes and seconds with leading zeros, if required
    hours = pretty_time_string(hours);
    minutes = pretty_time_string(minutes);
    seconds = pretty_time_string(seconds);

    // Compose the string for display
    var currentTimeString = hours + ":" + minutes + ":" + seconds;

    return currentTimeString;
  }
  //end of pretty looking clock

  //This counts time since the session starts
  var elapsed_seconds;
  chrome.storage.sync.get('elapsed_seconds', function(time) {
    // $('#total').text(budget.total);
    elapsed_seconds = time.elapsed_seconds;
    // alert(elapsed_seconds);
  })
  setInterval(function() {
    elapsed_seconds += 1;
    chrome.storage.sync.set({
      'elapsed_seconds': elapsed_seconds
    });
    $('#total_time').text(get_elapsed_time_string(elapsed_seconds));
  }, 1000);

  var left_seconds = 2;
  var refreshId = function(left_seconds) {
    var killInterval = setInterval(function() {
      left_seconds -= 1;
      $('#time_left').text(get_elapsed_time_string(left_seconds));
      if (left_seconds <= 80) {
        $('#time_left').css('color', 'red');
      }
      if (left_seconds < 1) {
        //this will kill the countdown timer
        clearInterval(killInterval);
        console.log("Time is up cowboy!!");
      }
    }, 1000);
  };
  //call to countdown timer with left time
  refreshId(left_seconds);
  //whenever user adds snooze time the counter is restarted
  $("#snooze_amount").click(function() {
    left_seconds += $("#snooze_time").val() * 60;
    refreshId(left_seconds);
  });
});
